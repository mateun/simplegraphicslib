#include "stdafx.h"
#include "Backbuffer.h"


Backbuffer::Backbuffer(HWND hwnd, unsigned int width, unsigned int height, unsigned int bitsPerPixel) : hwnd(hwnd)
{
	bitmapInfo.bmiHeader.biBitCount = bitsPerPixel;
	bitmapInfo.bmiHeader.biCompression = BI_RGB;
	bitmapInfo.bmiHeader.biPlanes = 1;
	bitmapInfo.bmiHeader.biHeight = height;
	bitmapInfo.bmiHeader.biWidth = width;
	bitmapInfo.bmiHeader.biSize = sizeof(bitmapInfo.bmiHeader);
	bitmapInfo.bmiHeader.biSizeImage = 0;
	bitmapInfo.bmiHeader.biXPelsPerMeter = 0;
	bitmapInfo.bmiHeader.biYPelsPerMeter = 0;
	bitmapInfo.bmiHeader.biClrUsed = 0;
	bitmapInfo.bmiHeader.biClrImportant = 0;

	int bytesPerPixel = 3;
	int bitmapMemorySize = bitmapInfo.bmiHeader.biWidth * bitmapInfo.bmiHeader.biHeight * bytesPerPixel;
	bytes = VirtualAlloc(0, bitmapMemorySize, MEM_COMMIT, PAGE_READWRITE);
	hdc = GetDC(hwnd);
}


Backbuffer::~Backbuffer()
{
	ReleaseDC(hwnd, hdc);
}

void * Backbuffer::getBytes()
{
	return bytes;
}

void Backbuffer::flip()
{

	StretchDIBits(hdc, 0, 0, bitmapInfo.bmiHeader.biWidth, bitmapInfo.bmiHeader.biHeight, 0, 0, bitmapInfo.bmiHeader.biWidth, bitmapInfo.bmiHeader.biHeight, bytes, &bitmapInfo, DIB_RGB_COLORS, SRCCOPY);

}
