#pragma once
class Backbuffer
{
public:
	Backbuffer(HWND hwnd, unsigned int width, unsigned int height, unsigned int bitsPerPixel);
	~Backbuffer();
	void* getBytes();
	void flip();

private:
	HWND hwnd;
	BITMAPINFO bitmapInfo;
	void* bytes;
	HDC hdc;
};

